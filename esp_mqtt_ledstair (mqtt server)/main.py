import time
import paho.mqtt.client as paho
import random
import codecs
import os

clear_console = lambda: os.system('clear')

rangefinder_left = {
    "topic": "1410120405/led_stairs/front",
    "value": 0
}

rangefinder_right = {
    "topic": "1410120405/led_stairs/rear",
    "value": 0
}


color_led = {
    "null": "000000",
    "red": "110000",
    "green": "001100",
    "blue": "000011",
}

def generate_led(leds: list):
    result = ""

    if len(leds) < 15:
        for i in range(15 - len(leds)):
            leds.append(color_led["null"])

    for byte in leds:
        result += byte

    return bytes.fromhex(result)

def run_led(client):
    color = [color_led["null"] for i in range(15)]
    if 44 >= rangefinder_right["value"] > 40:
        color[0] = color_led["red"]
        color[1] = color_led["red"]
        color[2] = color_led["red"]
    if 40 >= rangefinder_right["value"] > 30:
        color[3] = color_led["red"]
        color[4] = color_led["red"]
        color[5] = color_led["red"]
    if 30 >= rangefinder_right["value"] > 20:
        color[6] = color_led["red"]
        color[7] = color_led["red"]
        color[8] = color_led["red"]
    if 20 >= rangefinder_right["value"] > 10:
        color[9] = color_led["red"]
        color[10] = color_led["red"]
        color[11] = color_led["red"]
    if 10 >= rangefinder_right["value"] > 0:
        color[12] = color_led["red"]
        color[13] = color_led["red"]
        color[14] = color_led["red"]

    if 44 >= rangefinder_left["value"] > 40:
        color[14] = color_led["blue"]
        color[13] = color_led["blue"]
        color[12] = color_led["blue"]
    if 40 >= rangefinder_left["value"] > 30:
        color[11] = color_led["blue"]
        color[10] = color_led["blue"]
        color[9] = color_led["blue"]
    if 30 >= rangefinder_left["value"] > 20:
        color[8] = color_led["blue"]
        color[7] = color_led["blue"]
        color[6] = color_led["blue"]
    if 20 >= rangefinder_left["value"] > 10:
        color[5] = color_led["blue"]
        color[4] = color_led["blue"]
        color[3] = color_led["blue"]
    if 10 >= rangefinder_left["value"] > 0:
        color[2] = color_led["blue"]
        color[1] = color_led["blue"]
        color[0] = color_led["blue"]
    # if 40 > rangefinder_right["value"] < 30:
    #     color = generate_led([color_led["null"], color_led["null"], color_led["null"], color_led["red"], color_led["red"], color_led["red"]])

    client.publish("isu8266-00/lab/leds/strip/set_leds_bytes", generate_led(color))


def on_message(client, userdata, message):
    # time.sleep(1)
    data = str(message.payload.decode("utf-8"))
    clear_console()

    print("-" * 80)

    if message.topic == rangefinder_left["topic"]:
        rangefinder_left["value"] = int(data)
    
    if message.topic == rangefinder_right["topic"]:
        rangefinder_right["value"] = int(data)

    print("rangefinder_left =", rangefinder_left["value"])
    print("rangefinder_right =", rangefinder_right["value"])

    print("-" * 80)

    run_led(client)

if __name__ == "__main__":
    broker="broker.emqx.io"

    client= paho.Client("isu100123") 
    client.on_message=on_message

    print("Connecting to broker",broker)
    client.connect(broker) 
    client.loop_start()

    print("Subcribing")
    
    client.subscribe(rangefinder_left["topic"])
    client.subscribe(rangefinder_right["topic"])

    time.sleep(1800)
    client.disconnect()
    client.loop_stop()