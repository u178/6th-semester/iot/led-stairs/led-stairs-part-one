# LED stairs assignment

Create led stairs

## Part 1

set up network settings in Config.h in

- ssidCLI - network loging
- passwordCLI - network password
Change the first topic in ultrasonic_sender.ino
Use two esp8266 with last topic front or rear.

---

Change topics in esp_mqtt_ledstair (mqtt server)/main.py.

Don't forget to calibrate your ultrasonic distance meters and configure run_led file

Demo:

<https://youtube.com/shorts/b658Uob80i4>
